def zeroes(number, name):
    import os
    sufuri = '0'
    moja = '1'
    for pos in range(0, number) :
        sufuri += sufuri
        moja += moja

    string = sufuri + moja

    mode = 'w'
    if (os.path.exists(f'{os.getcwd()}/{name}.txt') and os.path.isfile(f'{os.getcwd()}/{name}.text')) :
        mode = 'a'

    file = open(f'{name}.txt', mode)
    file.write(string)
    file.close();
